let defaultSettings = {
	"scaleInitial": 2.5,
	"momentum": 6.0,
	"gravity": 0,
	"friction": 0.9999,
	"numParticles": 120
};

let images = {
	"scaleInitial": 1,
	"momentum": 7,
	"numParticles": 100
};

module.exports = {
	defaultSettings,
	images
};
